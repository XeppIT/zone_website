class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	
	layout :layout_by_resource

	private
	def layout_by_resource
		if devise_controller?
			"devise"
		else
			"application"
		end
	end

	def after_sign_in_path_for(resource)
		home_dashboard_url
	end

	def after_sign_out_path_for(resource)
		root_url
	end
end
